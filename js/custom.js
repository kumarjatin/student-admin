/*View Full Screen*/
var viewFullScreen = document.getElementById("view-fullscreen");
if (viewFullScreen) {
    viewFullScreen.addEventListener("click", function () {
        var docElm = document.documentElement;
        if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        }
        else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        }
    }, false);
}
/*Color Switcher*/
$('#grayButton').click(switchGray);
$('#redButton').click(switchRed);
$('#blueButton').click(switchBlue);
$('#yellowButton').click(switchYellow);
function switchGray() {
    $('body').attr('class', 'gray');
}
function switchRed() {
    $('body').attr('class', 'red');
}
function switchBlue() {
    $('body').attr('class', 'blue');
}
function switchYellow() {
    $('body').attr('class', 'yellow');
}
/*swatch toggle*/
$(document).ready(function(){
    $("#switcher-btn").click(function(){
        $("#switcher").toggle("fast");
    });
});
/*fullscreen div*/
$('#fullscrean-div').click(function(e){
    $('.fullscrean-div').toggleClass('fullscreen');
});
/*Responsive Tabs*/
$('.responsive-tabs').responsiveTabs({
    accordionOn: ['xs', 'sm']
});
/*btn fix on scroll*/
$(document).ready(function() {
    $('.btn-fix')
        .theiaStickySidebar({
            additionalMarginTop: 30
        });
});
